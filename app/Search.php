<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $fillable = ['city', 'lat', 'lon', 'temp', 'cloudy', 'wind', 'desc'];
}
