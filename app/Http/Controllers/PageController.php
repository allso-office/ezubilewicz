<?php

namespace App\Http\Controllers;

use App\Search;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('index.index');
    }

    public function stats()
    {
        $lasts = Search::orderBy('id', 'desc')->limit('10')->get();
        $allSearches = Search::all()->count();
        $lowestTemp = Search::all()->min('temp');
        $highestTemp = Search::all()->max('temp');
        $avgTemp = Search::all()->avg('temp');
        $generalyCities = Search::select('city')
                        ->groupBy('city')
                        ->orderByRaw('COUNT(*) DESC')
                        ->first();
        return view('stats.index')
            ->with('generalyCities', $generalyCities->city)
            ->with('lowestTemp', $lowestTemp)
            ->with('highestTemp', $highestTemp)
            ->with('allSearches', $allSearches)
            ->with('avgTemp', $avgTemp)
            ->with('lasts', $lasts);
    }
}
