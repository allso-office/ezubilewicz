<?php

namespace App\Http\Controllers;

use App\Search;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function store(Request $request)
    {
        $search = new Search;
        $search->create($request->all());
        $now = Carbon::now();
        return response()
            ->json([
                'status' => 'ok',
                'time' => date('H:i d.m.Y', strtotime($now))
            ]);
    }
}
