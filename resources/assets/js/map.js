const loadGoogleMapsApi = require('load-google-maps-api');

loadGoogleMapsApi().then(function (googleMaps) {
    let map = new googleMaps.Map(document.querySelector('#map'), {
        center: {
            lat: 52.226890665850775,
            lng: 20.99864959887657
        },
        zoom: 5,
    });
    googleMaps.event.addListener(map, "click", function (e) {
        showLoader();
        let lat = e.latLng.lat(),
            lon = e.latLng.lng();
        getWeatherInfo(lat, lon);

    });
}).catch(function (error) {
    $("#wetherModalLongTitle").text("Wystąpił błąd mapy");
    $('#wetherModal').modal('show');
});

function saveWeatherInfo(weather) {
    $.post( "/api/saveSearch", {
        city: weather.city,
        lat: weather.lat,
        lon: weather.lon,
        temp: weather.temp,
        cloudy: weather.cloudy,
        wind: weather.wind,
        desc: weather.desc
    })
        .done(function( data ) {
            if(data.status == "ok")
            {
                showModal(weather, data.time);

                hideLoader();
            }
            else{
                hideLoader();
            }
        });
}

function showModal(weather, currentTime) {
    $("#wetherModalLongTitle").text("Wybrana miejscowość: "+weather.city);
    $("#temp").text(weather.temp);
    $("#cloudy").text(weather.cloudy);
    $("#wind").text(weather.wind);
    $("#desc").text(weather.desc);
    $("#currentTime").text(currentTime);
    $('#wetherModal').modal('show');
}

function showLoader() {
    $('.loader').addClass('active');
}

function hideLoader() {
    $('.loader').removeClass('active');
}

function getWeatherInfo(lat, lon) {
    $.get( "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=765557fd6310de56497f48e5d364fbe0&units=metric&lang=pl")
        .done(function( data ) {
            let city = "";
            if(data.name) city = data.name;
            else city = "Nie można określić";
            let weather = {
                city: city,
                lat: data.coord.lat,
                lon: data.coord.lon,
                temp: data.main.temp,
                cloudy: data.clouds.all,
                wind: data.wind.speed,
                desc: data.weather[0].description
            };
            saveWeatherInfo(weather);
        });
}

