<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('style')
</head>
<body>
    @include('master.navBar')
@yield('head')
@yield('content')
    <div class="loader">
        <p>Proszę czekać</p>
        <span></span>
    </div>
@yield('footer')
@yield('script')
</body>
</html>