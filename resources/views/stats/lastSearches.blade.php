<h3 class="text-center pt-5">10 ostatnich wyszukiwań</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Miejscowość</th>
        <th scope="col">Temp. (&#176;C)</th>
        <th scope="col">Zachm. (%)</th>
        <th scope="col">Wiatr (m/s)</th>
        <th scope="col">Opis</th>
        <th scope="col">Data</th>
    </tr>
    </thead>
    <tbody>
    @foreach($lasts as $nr => $last)
        @php
        $nr++;
        @endphp
        <tr>
            <th scope="row">{{$nr}}</th>
            <td>{{$last->city}}</td>
            <td>{{$last->temp}}</td>
            <td>{{$last->cloudy}}</td>
            <td>{{$last->wind}}</td>
            <td>{{$last->desc}}</td>
            <td>{{$last->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>