@extends('master')
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('css/stats.css') }}">
@endsection
@section('content')
    <div class="container">

        @include('stats.lastSearches')
        @include('stats.stats')
    </div>
@endsection

