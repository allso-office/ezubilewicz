<h3 class="text-center pt-5">Statystyki</h3>
<div class="row">
    <div class="col-md-6 mx-auto">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Wartość</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Łącznie wyszukiwań</th>
                <td>{{$allSearches}}</td>
            </tr>
            <tr>
                <th scope="row">Minimalna temp.</th>
                <td>{{$lowestTemp}} &#176;C</td>
            </tr>
            <tr>
                <th scope="row">Maksymalna temp.</th>
                <td>{{$highestTemp}} &#176;C</td>
            </tr>
            <tr>
                <th scope="row">Średnia temp.</th>
                <td>{{$avgTemp}} &#176;C</td>
            </tr>
            <tr>
                <th scope="row">Njszęściej wyszukiwane miasto</th>
                <td>{{$generalyCities}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
