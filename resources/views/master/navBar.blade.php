<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/">Weather</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuToggle" aria-controls="menuToggle" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="menuToggle">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item @if(Route::currentRouteName() == 'index') active @endif">
                    <a class="nav-link" href="{{route('index')}}">Home</a>
                </li>
                <li class="nav-item @if(Route::currentRouteName() == 'stats') active @endif">
                    <a class="nav-link" href="{{route('stats')}}">Statystyki</a>
                </li>
            </ul>
        </div>

    </div>
</nav>