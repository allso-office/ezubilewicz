@extends('master')
@section('style')
    <link rel="stylesheet" href="{{ URL::asset('css/index.css') }}">
@endsection
@section('content')
    <main>
        @include('index.map')
        @include('index.modal')
    </main>
@endsection
@section('script')
    <script async src="{{ URL::asset('js/index.js') }}"></script>
@endsection
