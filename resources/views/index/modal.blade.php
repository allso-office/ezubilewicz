<div class="modal fade" id="wetherModal" tabindex="-1" role="dialog" aria-labelledby="wetherModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wetherModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover">

                    <tbody>
                    <tr>
                        <th scope="row">Temperatura: </th>
                        <td><span id="temp"></span>&#176;C</td>
                    </tr>
                    <tr>
                        <th scope="row">Zachmurzenie:</th>
                        <td><span id="cloudy"></span>%</td>
                    </tr>
                    <tr>
                        <th scope="row">Wiatr:</th>
                        <td><span id="wind"></span> m/s</td>
                    </tr>
                    <tr>
                        <th scope="row">Opis:</th>
                        <td id="desc"></td>
                    </tr>
                    <tr>
                        <th scope="row">Czas:</th>
                        <td id="currentTime"></td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
            </div>
        </div>
    </div>
</div>